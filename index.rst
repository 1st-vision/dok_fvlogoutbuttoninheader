.. |label| replace:: Logoutbutton

|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: 1st Vision GmbH
:Entwickler: Alexej Pavlov
:PHP: 7.0
:Kürzel: FvLogoutButtonInHeader
:Shopware-Version: 5.3.4
:Version: 1.0.1

Beschreibung
------------
Im Shopware-Standard ist das Abmelden nur unter dem Menüpunkt „Mein Konto“ möglich. 
Mit diesem Plugin wird ein Logout-Button immer im Header auf fast jeder Seite des Shops angezeigt. 
Dies ermöglicht dem Kunden jederzeit ein bequemes abmelden, intuitiv wie er es von anderen Webseiten gewohnt ist.

Frontend
--------
Im Header wird ein Button zwischen "Mein Konto" und dem Warenkorbbutton angezeigt.
Es wird als Tooltip der Text "Abmelden" eingeblendet und es kann auch jederzeit ein beliebes zusätzliches Text angezeigt werden.

.. image:: logoutbutton.png

Backend
-------
Es werden 2 neue Textbausteine angelegt

:Namespace: widgets/plugins/checkout/info
:Name: LogoutInHeaderTooltip
:Wert: Abmelden

:Namespace: widgets/plugins/checkout/info
:Name: LogoutInHeader
:Wert: <leer>

technische Beschreibung
------------------------
keine

Modifizierte Template-Datein
----------------------------
widget/plugins/checkout/info.tpl

